from django import template

register = template.Library()

# @register.simple_tag(takes_context=True)
# def perm_view(context):
#     if context['perm'] in context['']:
#         return 'True'

@register.simple_tag(takes_context=True)
def perm_add(context):
    a = []
    for perm in context['perm']:
        if context['add'] == perm:
            a.append(perm)
    return a

@register.simple_tag(takes_context=True)
def perm_change(context):
    a = []
    for perm in context['perm']:
        if context['change'] == perm:
            a.append(perm)
    return a

@register.simple_tag(takes_context=True)
def perm_delete(context):
    a = []
    for perm in context['perm']:
        if context['delete'] == perm:
            a.append(perm)
    return a