from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Book
from .forms import BookForm
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.models import User
from card.models import Card
from rest_framework import viewsets
from .serializer import BookSerializer


# Create your views here.
class BookListView(ListView):
    model = Book
    template_name = "list.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Каталог книг'
        context['action'] = 'КАТАЛОГ КНИГ'
        context['add'] = 'books.add_book'
        context['change'] = 'books.change_book'
        context['delete'] = 'books.delete_book'
        context['perm'] = self.request.user.get_all_permissions()
        card_id = self.request.session.get('card_id')
        if not card_id:
            if self.request.user.is_anonymous:
                user = User.objects.get(pk=4)
            else:
                user = self.request.user
            card = Card.objects.create(user=user)
            self.request.session['card_id'] = card.pk
        return context


class BookDetailView(DetailView):
    model = Book
    template_name = "detail.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Детали объекта: книга'
        context['action'] = 'Детализированная информация о книге'
        context['url'] = '/book'
        context['add'] = 'books.add_book'
        context['change'] = 'books.change_book'
        context['delete'] = 'books.delete_book'
        context['perm'] = self.request.user.get_all_permissions()
        return context


class BookCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'books.add_book'
    model = Book
    form_class = BookForm
    success_url = '/book/list'
    template_name = "mainCRUD/create.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Создание объекта: книга'
        context['action'] = 'Введение информации о новой книге'
        context['url'] = '/book'
        context['perm'] = '_book'
        return context


class BookUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'books.change_book'
    model = Book
    form_class = BookForm
    template_name = "mainCRUD/create.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Редактирование объекта: книга'
        context['action'] = 'Редактирование существующего объекта: книга'
        context['url'] = '/book'
        context['perm'] = '_book'
        return context


class BookDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'books.delete_book'
    model = Book
    success_url = '/book/list'
    template_name = "mainCRUD/delete.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Удаление объекта: книга'
        context['action'] = 'Удаление существующего объекта: книга'
        context['url'] = '/book'
        context['perm'] = '_book'
        return context


class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer