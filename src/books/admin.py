from django.contrib import admin
from .models import Book


# Register your models here.
class BookAdmin(admin.ModelAdmin):

    list_display = [
        'name',
        'price',
        'series',
        'year',
        'pages',
        'cover',
        'form',
        'weight', #+' kg',
        'age_limit',#+'+',
        'publisher',
        'available_PCs',#+' PCs',
        'available_bool',
        'rank',
        'created',
        'updated'
    ]

admin.site.register(Book, BookAdmin)