from django.forms import ModelForm
from .models import Book


# Create the form class.
class BookForm(ModelForm):
    class Meta:
        model = Book
        fields = '__all__'
        initial = {}
        labels = {
            'name': 'Название',
            'photo': 'Обложка',
            'price': 'Цена, BYN',
            'author': 'Автор(ы)',
            'series': 'Серия',
            'genre': 'Жанр(ы)',
            'year': 'Год первоиздания',
            'pages': 'Кол-во страниц',
            'cover': 'Тип обложки',
            'form': 'Формат издания',
            'ISBN': 'ISBN', # HTML valid 978-x-xx-xxxxxx-x
            'weight': 'Вес, г',
            'age_limit': 'Возрастное ограничение',  # HTML with "' '+"
            'publisher': 'Издатель',
            'available_PCs': 'Доступный остаток, шт.',
            'available_bool': 'Наличие',
            'rank': 'Рейтинг',
            'created': 'Запись создана',
            'updated': 'Запись обновлена'
        }