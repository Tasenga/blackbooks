from django.db import models
from datetime import datetime
from refers.models import Genre, Publisher, Series, Author
from django.db.models.signals import pre_init
from django.dispatch import receiver


# Create your models here.
class Book(models.Model):
    def years_():
        years = []
        for year in range(1700, datetime.now().year + 3):
            years.append((year, str(year)))
        return years

    def pcs_():
        pcs = []
        for i in range(0, 101):
            pcs.append((i, str(i)))
        return pcs

    def rank_():
        rank = []
        for i in range(0, 11):
            rank.append((i, str(i)))
        return rank

    name = models.CharField(
        max_length=100
    )

    photo = models.ImageField(
        upload_to='books/static/',
        null=True,
        blank=True,
    )

    price = models.DecimalField(
        max_digits=6,
        decimal_places=2,
        default=1000.10
    )  # HTML with BYN

    author = models.ManyToManyField(
        Author,
        related_name='book',
        default=0
    )

    series = models.ForeignKey(
        Series,
        on_delete=models.CASCADE,
        related_name='book',
        null=True,
        blank=True
    )

    genre = models.ManyToManyField(
        Genre,
        related_name='book',
        default=0
    )

    year = models.IntegerField(
        choices=years_(),
        default=datetime.now().year,
        null=True,
        blank=True
    )

    pages = models.PositiveSmallIntegerField(
        default=10
    )

    cover = models.CharField(
        max_length=10,
        choices=[('hard', 'hardcover'),
                 ('soft', 'softcover')],
        default='softcover'
    )

    form = models.CharField(
        max_length=10,
        default='standard',
        choices=[
            ('pocket', '9x13 cm'),
            ('compact', '10.5x15.5 cm'),
            ('personal', '13x19.5 cm'),
            ('standard', '13.5x21.5 cm'),
            ('large', '17x24.4 cm'),
            ("reader's", '21.3x34.5 cm', )
        ]
    )

    ISBN = models.CharField(
        max_length=17,
        default='978-x-xx-xxxxxx-x'
    )  # HTML format 978-x-xx-xxxxxx-x

    weight = models.FloatField(
        default=0
    )  # HTML with "' ' g"

    age_limit = models.PositiveSmallIntegerField(
        default=0,
        choices=[
            (0, '0'),
            (6, '6'),
            (12, '12'),
            (16, '16'),
            (18, '18')]
    )  # HTML with "' '+"

    publisher = models.ForeignKey(
        Publisher,
        on_delete=models.CASCADE,
        related_name='book',
        null=True,
        blank=True
    )

    available_PCs = models.PositiveSmallIntegerField(
        default=1,
        choices=pcs_(),
        null=True,
        blank=True
    )  # HTML with 'PCs'

    available_bool = models.BooleanField(
        default=True
    )

    rank = models.PositiveSmallIntegerField(
        default=10,
        choices=rank_(),
        null=True,
        blank=True
    )

    created = models.DateTimeField(
        auto_now=False,
        auto_now_add=True
    )

    updated = models.DateTimeField(
        auto_now=True,
        auto_now_add=False
    )


    def __str__(self):
        return f'"{self.name}".  {self.list_author()}'

    def list_author(self):
        list_author = ''
        for author in self.author.all():
            list_author += author.name + ' ' + author.surname
        return list_author

    def get_absolute_url(self):
        return f"/book/{self.pk}/"
