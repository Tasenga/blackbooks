from django.urls import path, include
from .views import *
from django.conf.urls.static import static
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'books', BookViewSet)

urlpatterns = [
    path('book/list/', BookListView.as_view()),
    path('', BookListView.as_view()),
    path('book/<int:pk>/', BookDetailView.as_view()),
    path('book/create/', BookCreateView.as_view()),
    path('book/update/<int:pk>/', BookUpdateView.as_view()),
    path('book/delete/<int:pk>/', BookDeleteView.as_view()),
    path('rest/', include(router.urls))
]