from django.views.generic.edit import UpdateView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView
from django.views.generic.detail import DetailView
from django.core.mail import send_mail
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from .models import BookInCard, Card, Order
from books.models import Book
from .forms import *
from .utils import get_price
from random import choice


# BOOKINCARD
class BookInCardUpdateView(UpdateView):
    model = BookInCard
    form_class = BookInCardForm
    template_name = "create_card.html"
    success_url = reverse_lazy('card:list')

    def get_object(self):
        book_to_add = self.kwargs.get('pk')
        card_id = self.request.session.get('card_id')
        if not card_id:
            if self.request.user.is_anonymous:
                user = User.objects.get(pk=4)
            else:
                user = self.request.user
            card = Card.objects.create(user=user)
            self.request.session['card_id'] = card.pk
        else:
            card = Card.objects.get(pk=card_id)
        book = Book.objects.get(pk=book_to_add)
        book_in_card, created = BookInCard.objects.get_or_create(
            book=book,
            card=card,
            defaults={'price': book.price}
        )
        if not created:
            book_in_card.quantity = book_in_card.quantity + 1
            book_in_card.price = book.price * book_in_card.quantity
        return book_in_card

    def form_valid(self, form, **kwargs):
        quantity = form.cleaned_data.get('quantity')
        price = self.object.book.price
        total_price = quantity * price
        self.object.price = total_price
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class BookInCardDeleteView(DeleteView):
    model = BookInCard
    success_url = reverse_lazy('card:list')

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


class BookInCardDetailView(DetailView):
    model = BookInCard
    template_name = "detail_card.html"


# CARD
class CurrentCardListView(TemplateView):
    model = Card
    template_name = "list_current_card.html"
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        card_id = self.request.session.get('card_id')
        card = None
        if card_id:
            card = Card.objects.get(pk=card_id)

        # для предложения доп.книг пользователю
        if len(card.books.all()) > 0:
            genre_list = []
            for book in card.books.all():
                for genre in book.book.genre.all():
                    genre_list.append(genre)
            context['genre'] = choice(genre_list)
            author_list = []
            for book in card.books.all():
                for author in book.book.author.all():
                    author_list.append(author)
            context['author'] = choice(author_list)
        # ____________________________________

        context['card'] = card
        context['title'] = 'Корзина для заказа'
        context['action'] = 'Пожалуйста, проверьте Ваш заказ'
        context['url'] = '/card'
        context['delivery_price'] = get_price(card.total_price())[1]
        return context

class CardDetailView(DetailView):
    model = Card
    template_name = "list_card.html"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title'] = 'Корзина для заказа'
        context['action'] = 'Пожалуйста, проверьте Ваш заказ'
        return context


# ORDER
class OrderUpdateView(UpdateView):
    model = Order
    form_class = OrderForm
    template_name = "order_create.html"
    success_url = '/book/list'

    def get_object(self):
        card_id = self.request.session.get('card_id')
        card = Card.objects.get(pk=card_id)
        user = card.user
        order, created = Order.objects.get_or_create(
            card=card,
            defaults={'price': get_price(card.total_price())[0],
                      'delivery_price': get_price(card.total_price())[1],
                      'delivery_address': user.person.location}
        )
        if created:
            self.request.session['order_id'] = order.pk
            order.price = get_price(card.total_price())[0]
            order.delivery_price = get_price(card.total_price())[1]
            order.delivery_address = user.person.location
            order.save()
        if not created:
            order.price = get_price(card.total_price())[0]
            order.delivery_price = get_price(card.total_price())[1]
            order.delivery_address = user.person.location
            order.save()
        return order

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'Заполните необходимые поля для доставки'
        context['title'] = 'Оформление заказа'
        return context

    def get_success_url(self):
        order = Order.objects.get(pk=self.request.session.get('order_id'))
        for book in order.card.books.all():
            book.book.available_PCs = book.book.available_PCs - book.quantity
            book.book.save()
        self.request.session['order_id'] = None
        self.request.session['card_id'] = None
        if not self.request.user.is_anonymous:
            send_mail(
                'Подтверждение заказа',
                'Ваш заказ получен, укомплектован и будет отправлен',
                'genuine_fake@mail.ru',
                [self.request.user.email]
            )
        return super().get_success_url()

class OrderListView(ListView):
    model = Order
    template_name = 'order_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action'] = 'История заказов'
        context['title'] = 'Заказы'
        return context
    def get_queryset(self):
        if self.queryset is not None:
            queryset = self.queryset
            if isinstance(queryset, QuerySet):
                queryset = queryset.filter(card__user=self.request.user)
        elif self.model is not None:
            queryset = self.model._default_manager.filter(card__user=self.request.user)
        else:
            raise ImproperlyConfigured(
                "%(cls)s is missing a QuerySet. Define "
                "%(cls)s.model, %(cls)s.queryset, or override "
                "%(cls)s.get_queryset()." % {
                    'cls': self.__class__.__name__
                }
            )
        ordering = self.get_ordering()
        if ordering:
            if isinstance(ordering, str):
                ordering = (ordering,)
            queryset = queryset.order_by(*ordering)
        return queryset