from django.forms import ModelForm
from .models import BookInCard, Order
from books.models import Book
from django.forms.models import model_to_dict


class BookInCardForm(ModelForm):
    class Meta:
        model = BookInCard
        fields = ['quantity', ]
        labels = {'quantity': 'Укажите количество экземпляров, которое хотите заказать', }

    available_PCs = -1

    def __init__(self, instance=None, *args, **kwargs):
        data_from_book = model_to_dict(instance.book, 'available_PCs')
        self.available_PCs = data_from_book.get('available_PCs')
        super(BookInCardForm, self).__init__(instance=instance, *args, **kwargs)

    def clean(self, instance=None, *args, **kwargs):
        cleaned_data = super().clean()
        quantity = cleaned_data.get("quantity")
        if quantity > self.available_PCs:
            msg = "Такого количесва экземпляров выбранной книги нет в наличии"
            self.add_error('quantity', msg)

class OrderForm(ModelForm):
    class Meta:
        model = Order
        fields = ['delivery_address', ]
