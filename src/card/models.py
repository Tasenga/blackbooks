from django.db import models
from django.contrib.auth import get_user_model
from books.models import Book

User = get_user_model()

class Card(models.Model):
    user = models.ForeignKey(
        User,
        verbose_name="Клиент",
        related_name="cards",
        on_delete=models.PROTECT
    )

    def total_price(self):
        books = self.books.all()
        total = 0
        for book in books:
            total += book.price
        return total

class BookInCard(models.Model):
    book = models.ForeignKey(
        Book,
        verbose_name="Книга",
        related_name="books",
        on_delete=models.PROTECT
    )
    card = models.ForeignKey(
        Card,
        verbose_name="Корзина",
        related_name="books",
        on_delete=models.PROTECT
    )
    quantity = models.PositiveSmallIntegerField(
        verbose_name="Количество",
        default=1,
        null=False
    )
    price = models.DecimalField(
        max_digits=8,
        decimal_places=2
    )

    def price_for_one(self):
        price_for_one = self.price / self.quantity
        return price_for_one

    def __str__(self):
        return f"{self.book.name} * {self.quantity}"


class Order(models.Model):
    card = models.OneToOneField(
        Card,
        verbose_name="Корзина",
        related_name="order",
        on_delete=models.PROTECT
    )
    price = models.DecimalField(
        verbose_name="Стоимость заказа",
        max_digits=8,
        decimal_places=2
    )
    delivery_price = models.DecimalField(
        verbose_name="Цена доставки",
        max_digits=8,
        decimal_places=2
    )
    delivery_address = models.TextField(
        verbose_name="Адрес доставки",
    )
    status = models.CharField(
        max_length=20,
        default='в обработке',
        choices=[
            ('в обработке', 'в обработке'),
            ('передан курьеру', 'передан курьеру'),
            ('выполнен', 'выполнен'),
            ('отменен', 'отменен')
        ]
    )
    created = models.DateTimeField(
        auto_now=False,
        auto_now_add=True
    )
    updated = models.DateTimeField(
        auto_now=True,
        auto_now_add=False
    )