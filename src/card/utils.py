from decimal import Decimal, ROUND_HALF_UP

def get_price(price):
    if price >= 100:
        delivery_price = Decimal(Decimal(price) * Decimal('0.02')).quantize(Decimal('.01'), rounding=ROUND_HALF_UP)
        return price, delivery_price
    else:
        delivery_price = Decimal(Decimal(price) * Decimal('0.1')).quantize(Decimal('.01'), rounding=ROUND_HALF_UP)
        return price, delivery_price

