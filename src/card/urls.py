from django.urls import path
from .views import *


app_name = "card"
urlpatterns = [
    path('card/add/<int:pk>/', BookInCardUpdateView.as_view(), name="add"),
    path('card/update/<int:pk>/', BookInCardUpdateView.as_view()),
    path('bookincard/delete/<int:pk>/', BookInCardDeleteView.as_view()),
    path('bookincard/<int:pk>/', BookInCardDetailView.as_view()),
    path('card/', CurrentCardListView.as_view(), name="list"),
    path('card/<int:pk>/', CardDetailView.as_view()),
    path('checkout/<int:pk>', OrderUpdateView.as_view(), name="order"),
    path('order/list/', OrderListView.as_view())
]