from django.urls import path, include
from .views import *
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'tag', TagViewSet)
router.register(r'contenttype', ContentTypeViewSet)

urlpatterns = [
    path('search/?q', SearchResult.as_view(), name='search_results'),
    path('search/', SearchResult.as_view(), name='search_results'),
    path('tags/', index),
    path('rest/', include(router.urls))
]