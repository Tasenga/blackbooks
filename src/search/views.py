from django.shortcuts import render
from django.http import HttpResponse
from django.template import Context, loader
from django.views.generic import UpdateView
from django.views.generic.list import ListView
from .models import Tag
from django.contrib.contenttypes.models import ContentType
from .utils import searching
from rest_framework import viewsets
from .serializer import TagSerializer, ContentTypeSerializer


# Create your views here.
class SearchResult(ListView):
    model = Tag
    template_name = "search_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Поиск'
        context['action'] = 'РЕЗУЛЬТАТ ПОИСКА'
        return context

    def get_queryset(self):
        key_word = self.request.GET.get('q')
        queryset = searching(key_word, queryset=[])

        if len(queryset) == 0:
            queryset = []
            letter = 0
            for key_word in range(15):
                key_word = self.request.GET.get('q')[letter:letter+3]
                if len(key_word) >= 3:
                    searching(key_word, queryset)
                    letter += 3
        return queryset


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer

class ContentTypeViewSet(viewsets.ModelViewSet):
    queryset = ContentType.objects.all()[6:11]
    serializer_class = ContentTypeSerializer

# class TagViewSet_for_staff(UpdateView):
#     model = Tag
#     template_name = "tag_viewset.html"
#     fields = []
#     def get_object(self):
#         return None

def index(request):
    template = loader.get_template("tag_viewset.html")
    return HttpResponse(template.render())
