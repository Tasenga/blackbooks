from .models import Tag
from rest_framework import serializers
from django.contrib.contenttypes.models import ContentType


class ContentTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContentType
        fields = ['pk', 'app_label', 'model']

class TagSerializer(serializers.ModelSerializer):
    content_type_obj = ContentTypeSerializer(source='content_type', read_only=True)
    content_object = serializers.StringRelatedField(many=False)
    class Meta:
        model = Tag
        fields = ['pk', 'tag', 'content_type', 'content_type_obj', 'object_id', 'content_object']