from books.models import Book
from refers.models import Genre, Publisher, Series, Author
from .models import Tag
from django.db.models import Q


def searching(key_word, queryset):

    def filling_queryset(queryset, list):
        for obj in list:
            # проверка уникальности поискового результата
            check_name = False
            for i in queryset:
                if i[1].find(str(obj)) != -1:
                    check_name = True

            # определение наименования модели (для генерации URL)
            model = obj.__class__.__name__.lower()
            if model == "tag":
                model = str(obj.content_type)

            # определение наименования раздела
            if model == 'author':
                chapter = 'АВТОРЫ'
            elif model == 'book':
                chapter = 'КНИГИ'
            elif model == 'series':
                chapter = 'СЕРИИ'
            elif model == 'genre':
                chapter = 'ЖАНРЫ'
            elif model == 'publisher':
                chapter = 'ИЗДАТЕЛЬСТВА'

            # добавление поискового результата в список
            if check_name == False:
                queryset.append(
                    ['В разделе {}:'.format(chapter),
                     '{}'.format(str(obj)),
                     obj.get_absolute_url()]
                )
        return queryset


    # если запрос пустой
    if not key_word:
        queryset = filling_queryset(queryset, Book.objects.all())

    # если запрос не пустой
    else:
        # 1. если запросом подразумевается str:
        # 1.1. дополнение queryset книгами
        list_from_books = Book.objects.filter(
            Q(name__icontains=key_word) |
            Q(cover__icontains=key_word) |
            Q(form__icontains=key_word)
        )
        filling_queryset(queryset, list_from_books)

        # 1.2. дополнение queryset авторами
        list_from_authors = Author.objects.filter(
            Q(name__icontains=key_word) |
            Q(surname__icontains=key_word) |
            Q(nationality__icontains=key_word)
        )
        filling_queryset(queryset, list_from_authors)
        if len(key_word) >= 3:
            filling_queryset(queryset, Author.objects.filter(biography__icontains=key_word))

        # 1.3. дополнение queryset сериями
        list_from_series = Series.objects.filter(
            Q(name__icontains=key_word)
        )
        filling_queryset(queryset, list_from_series)

        # 1.4. дополнение queryset жанрами
        list_from_genre = Genre.objects.filter(
            Q(name__icontains=key_word)
        )
        filling_queryset(queryset, list_from_genre)

        # 1.5. дополнение queryset издательствами
        list_from_publisher = Publisher.objects.filter(
            Q(name__icontains=key_word) |
            Q(email__icontains=key_word)
        )
        filling_queryset(queryset, list_from_publisher)

        # 1.6. дополнение queryset по #tag
        list_from_tags = Tag.objects.filter(
            Q(tag__icontains=key_word)
        )
        filling_queryset(queryset, list_from_tags)

        # 2. если запросом подразумевается int (для поиска по году)
        # проверка возможности перевода в int

        if key_word.isdigit() and len(key_word) > 2:
            # 2.1. дополнение queryset по книгам
            list_from_books_for_int = Book.objects.filter(
                Q(year__icontains=key_word)
            )
            filling_queryset(queryset, list_from_books_for_int)

            # 2.2. дополнение queryset по книгам
            list_from_author_for_int = Author.objects.filter(
                Q(year_birth__icontains=key_word) |
                Q(year_death__icontains=key_word)
            )
            filling_queryset(queryset, list_from_author_for_int)

    return queryset