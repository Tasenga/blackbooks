from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models
from refers.models import Author


# Create your models here.
class Tag(models.Model):
    tag = models.CharField(max_length=30)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name='tag')
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return f"{str(self.content_object)}"

    def get_absolute_url(self):
        return f"/{str(self.content_type)}/{self.content_object.pk}/"

    @receiver(post_save, sender=Author)
    def create_tag_author_full_name(sender, instance, created, **kwargs):
        if created:
            tag = Tag.objects.create(
                tag=str(instance.name) + ' ' + str(instance.surname),
                content_object=instance
            )
            tag.save()