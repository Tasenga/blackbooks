from django.urls import path, include
from .views import *
from django.conf.urls.static import static
from books.views import BookListView

urlpatterns = [
    path('accounts/login/', LoginView.as_view()),
    path('accounts/password_change/done/', BookListView.as_view(), name='password_change'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/create/', UserCreateView.as_view()),
    path('person/<str:username>/', PersonDetailView.as_view(), name='person'),
    path('person/update/<str:username>/', PersonUpdateView.as_view()),
    path('person/clean_data/<str:username>/', PersonCleanData.as_view())
]