# Generated by Django 2.2.5 on 2019-10-15 22:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='person',
            name='created',
        ),
        migrations.RemoveField(
            model_name='person',
            name='updated',
        ),
        migrations.AddField(
            model_name='person',
            name='quote',
            field=models.TextField(blank=True, max_length=500, null=True),
        ),
    ]
