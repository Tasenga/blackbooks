from django import forms
from .models import Person
from refers.models import Genre, Author
from django.contrib.auth import models
from django.forms.models import model_to_dict, fields_for_model
from django.utils import formats


# Create the form class.
class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = (
            'location',
            'biography',
            'location',
            'birth_date',
            'sex',
            'photo',
            'phone',
            'quote',
            'prefer_genre',
            'prefer_author')


class UserForm(forms.ModelForm):
    class Meta:
        model = models.User
        fields = ('first_name', 'last_name', 'email')


class DateField(forms.DateField):
    input_formats = formats.get_format_lazy('DATE_INPUT_FORMATS')
    input_formats.append('%d.%m.%Y')


class UserPersonForm(forms.ModelForm):
    def __init__(self, instance=None, *args, **kwargs):
        _fields = ('first_name', 'last_name', 'email',)
        super(UserPersonForm, self).__init__(instance=instance, *args, **kwargs)
        self.fields.update(fields_for_model(models.User, _fields))

    class Meta:
        model = Person
        exclude = ['user', ]
        labels = {'biography': '',
                  'location': '',
                  'birth_date': '',
                  'sex': '',
                  'photo': '',
                  'phone': '',
                  'quote': 'Цитата',
                  'prefer_genre': '',
                  'prefer_author': ''
                  }

        help_texts = {
                  'prefer_genre': 'Вы можете добавить несколько любимых жанров, удерживая CTRL',
                  'prefer_author': 'Вы можете добавить несколько любимых авторов, удерживая CTRL '
                  }

    def save(self, *args, **kwargs):
        u = self.instance.user
        u.first_name = self.cleaned_data['first_name']
        u.last_name = self.cleaned_data['last_name']
        u.email = self.cleaned_data['email']
        u.save()
        person = super(UserPersonForm, self).save(*args, **kwargs)
        return person

class UserPersonCleanForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = []