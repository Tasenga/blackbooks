from django.db import models
from django.contrib.auth.models import User
from refers.models import Genre, Author
from django.db.models.signals import post_save
from django.dispatch import receiver

class Person(models.Model):
    user = models.OneToOneField(User, primary_key=True, related_name='person', on_delete=models.CASCADE)
    biography = models.TextField(max_length=500, null=True, blank=True)
    location = models.CharField(max_length=200, null=False, blank=False, default='Belarus')
    birth_date = models.DateField(default='1900-01-01', null=True, blank=True)
    sex = models.CharField(max_length=7, choices=[('мужской', 'мужской'), ('женский', 'женский')], null=True, blank=True)
    photo = models.ImageField(upload_to='users/static/', null=True, blank=True)
    phone = models.CharField(max_length=18, default='+375(__)-__-__-__', null=True, blank=True)
    quote = models.TextField(max_length=500, null=True, blank=True)
    prefer_genre = models.ManyToManyField(Genre, related_name='person', blank=True)
    prefer_author = models.ManyToManyField(Author, related_name='person', blank=True)

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Person.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.person.save()

    def get_absolute_url(self):
        return f"/person/{self.user.username}/"

    def __str__(self):
        return self.user.username
