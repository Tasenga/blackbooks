from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Person
from .forms import *
from django.contrib.auth import views as auth_views
from django.shortcuts import render_to_response


# USER CRUD
class UserCreateView(CreateView):
    model = User
    success_url = "/accounts/login/"
    template_name = "registration/registration.html"
    form_class = UserCreationForm


class LoginView(auth_views.LoginView):
    def get_success_url(self):
        self.request.session['order_id'] = None
        self.request.session['card_id'] = None
        return super().get_success_url()


# PERSON CRUD
class PersonDetailView(DetailView):
    model = Person
    template_name = "profile.html"

    def get_object(self):
        username = self.model.objects.get(user__username=self.request.user.username)
        return username


class PersonUpdateView(UpdateView):
    model = Person
    form_class = UserPersonForm
    template_name = "create.html"

    def get_object(self):
        username = self.model.objects.get(user__username=self.request.user.username)
        return username


class PersonCleanData(UpdateView):
    model = Person
    form_class = UserPersonCleanForm
    template_name = "profile_clean.html"

    def get_object(self):
        username = self.model.objects.get(user__username=self.request.user.username)
        return username

    def get_success_url(self):
        person = Person.objects.get(user=self.request.user)
        person.biography = None
        person.location = 'Belarus'
        person.birth_date = '1900-01-01'
        person.sex = None
        person.photo = None
        person.phone = '+375(__)-__-__-__'
        person.quote = None
        for prefer_author in person.prefer_author.all():
            person.prefer_author.remove(prefer_author)
        for prefer_genre in person.prefer_genre.all():
            person.prefer_genre.remove(prefer_genre)
        person.save()
        user = User.objects.get(pk=self.request.user.pk)
        user.first_name = ''
        user.last_name = ''
        user.email = ''
        user.save()
        return super().get_success_url()


def handler404(request, exception, template_name="404.html"):
    response = render_to_response("404.html")
    response.status_code = 404
    return response

def handler403(request, exception, template_name="403.html"):
    response = render_to_response("403.html")
    response.status_code = 403
    return response