from django.db import models
from datetime import datetime
from django_countries.fields import CountryField
# from django.contrib.contenttypes.fields import GenericRelation
# from search.models import Tag


class Genre(models.Model):
    name = models.CharField(
        unique=True,
        max_length=30,
        blank=False,
        null=False)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    def get_absolute_url(self):
        return f"/genre/{self.pk}/"
    def __str__(self):
        return self.name


class Publisher(models.Model):
    name = models.CharField(max_length=30, unique=True)
    email = models.EmailField(default='none@gmail.com', blank=True, null=True)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return f"/publisher/{self.pk}/"


class Series(models.Model):
    name = models.CharField(max_length=200, unique=True)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return f"/series/{self.pk}/"


class Author(models.Model):
    def year_birth():
        years = []
        for year in range(1700, datetime.now().year - 15):
            years.append((year, str(year)))
        return years

    def year_death():
        years = []
        for year in range(1700, datetime.now().year):
            years.append((year, str(year)))
        years.append(('', 'live'))
        return years

    name = models.CharField(
        max_length=30
    )
    surname = models.CharField(
        max_length=30
    )
    year_birth = models.SmallIntegerField(
        default=1990,
        choices=year_birth()
    )
    year_death = models.SmallIntegerField(
        default=datetime.now().year,
        choices=year_death(),
        null=True,
        blank=True)
    nationality = CountryField(
        null=True,
        blank=True
    )
    biography = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    created = models.DateTimeField(
        auto_now=False,
        auto_now_add=True
    )
    updated = models.DateTimeField(
        auto_now=True,
        auto_now_add=False
    )
    # tags = GenericRelation(Tag)

    def get_absolute_url(self):
        return f"/author/{self.pk}/"

    def __str__(self):
        return self.name + '  ' + self.surname