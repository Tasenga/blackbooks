from django.contrib import admin
from .models import Genre, Series, Publisher, Author

# Register your models here.
admin.site.register(Genre)
admin.site.register(Series)
admin.site.register(Publisher)
admin.site.register(Author)