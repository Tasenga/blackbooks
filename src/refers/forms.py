from django.forms import ModelForm
from .models import Genre, Author, Publisher, Series


# Create the form class.
class GenreForm(ModelForm):
    class Meta:
        model = Genre
        fields = ['name']


class AuthorForm(ModelForm):
    class Meta:
        model = Author
        fields = '__all__'
        initial = {
            'year_birth': '_1800',
            'year_death': '_None'}
        labels = {
            'name': 'Имя',
            'surname': 'Фамилия',
            'year_birth': 'Год рождения',
            'year_death': 'Год смерти',
            'nationality': 'Национальность',
            'biography': 'Дополнительная информация'}


class PublisherForm(ModelForm):
    class Meta:
        model = Publisher
        fields = '__all__'


class SeriesForm(ModelForm):
    class Meta:
        model = Series
        fields = ['name']
