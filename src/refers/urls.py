from django.urls import path, include
from .views import *
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'genres', GenreViewSet)
router.register(r'serieses', SeriesViewSet)
router.register(r'authors', AuthorViewSet)
router.register(r'publishers', PublisherViewSet)

urlpatterns = [
    path('genre/list/', GenreListView.as_view()),
    path('genre/<int:pk>/', GenreDetailView.as_view()),
    path('genre/create/', GenreCreateView.as_view()),
    path('genre/update/<int:pk>/', GenreUpdateView.as_view()),
    path('genre/delete/<int:pk>/', GenreDeleteView.as_view()),

    path('series/list/', SeriesListView.as_view()),
    path('series/<int:pk>/', SeriesDetailView.as_view()),
    path('series/create/', SeriesCreateView.as_view()),
    path('series/update/<int:pk>/', SeriesUpdateView.as_view()),
    path('series/delete/<int:pk>/', SeriesDeleteView.as_view()),

    path('author/list/', AuthorListView.as_view()),
    path('author/<int:pk>/', AuthorDetailView.as_view()),
    path('author/create/', AuthorCreateView.as_view()),
    path('author/update/<int:pk>/', AuthorUpdateView.as_view()),
    path('author/delete/<int:pk>/', AuthorDeleteView.as_view()),

    path('publisher/list/', PublisherListView.as_view()),
    path('publisher/<int:pk>/', PublisherDetailView.as_view()),
    path('publisher/create/', PublisherCreateView.as_view()),
    path('publisher/update/<int:pk>/', PublisherUpdateView.as_view()),
    path('publisher/delete/<int:pk>/', PublisherDeleteView.as_view()),

    path('rest/', include(router.urls))
]