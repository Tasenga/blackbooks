from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from rest_framework import viewsets
from django.contrib.auth.mixins import PermissionRequiredMixin

from .models import Genre, Author, Publisher, Series
from .forms import GenreForm, AuthorForm, PublisherForm, SeriesForm
from .serializer import GenreSerializer, AuthorSerializer, SeriesSerializer, PublisherSerializer




# GENRE CRUD
class GenreListView(ListView):
    model = Genre
    template_name = "refers/genre/list.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Список жанров'
        context['action'] = 'СПИСОК ЖАНРОВ'
        context['current_menu'] = 'Жанры'
        context['url'] = '/genre'
        context['add'] = 'refers.add_genre'
        context['change'] = 'refers.change_genre'
        context['delete'] = 'refers.delete_genre'
        context['perm'] = self.request.user.get_all_permissions()
        return context

class GenreDetailView(DetailView):
    model = Genre
    template_name = "refers/genre/detail.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Детали объекта: жанр'
        context['action'] = 'Детализированная информация о жанре'
        context['current_menu'] = 'Жанры'
        context['url'] = '/genre'
        context['add'] = 'refers.add_genre'
        context['change'] = 'refers.change_genre'
        context['delete'] = 'refers.delete_genre'
        context['perm'] = self.request.user.get_all_permissions()
        return context

class GenreCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'refers.add_genre'
    model = Genre
    form_class = GenreForm
    template_name = "mainCRUD/create.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Создание объекта: жанр'
        context['action'] = 'Введение информации о новом жанре'
        context['current_menu'] = 'Жанры'
        context['url'] = '/genre'
        return context

class GenreUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'refers.update_genre'
    model = Genre
    form_class = GenreForm
    template_name = "mainCRUD/create.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Редактирование объекта: жанр'
        context['action'] = 'Редактирование существующего объекта: жанр'
        context['current_menu'] = 'Жанры'
        context['url'] = '/genre'
        return context

class GenreDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'refers.delete_genre'
    model = Genre
    success_url = '/genre/list'
    template_name = "mainCRUD/delete.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Удаление объекта: жанр'
        context['action'] = 'Удаление существующего объекта: жанр'
        context['current_menu'] = 'Жанры'
        context['url'] = '/genre'
        return context

class GenreViewSet(viewsets.ModelViewSet):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer


# SERIES CRUD
class SeriesListView(ListView):
    model = Series
    template_name = "refers/series/list.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Список серий'
        context['action'] = 'СПИСОК СЕРИЙ'
        context['current_menu'] = 'Серии'
        context['url'] = '/series'
        context['add'] = 'refers.add_series'
        context['change'] = 'refers.change_series'
        context['delete'] = 'refers.delete_series'
        context['perm'] = self.request.user.get_all_permissions()
        return context

class SeriesDetailView(DetailView):
    model = Series
    template_name = "refers/series/detail.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Детали объекта: серия'
        context['action'] = 'Детализированная информация о серии'
        context['current_menu'] = 'Серии'
        context['url'] = '/series'
        context['add'] = 'refers.add_series'
        context['change'] = 'refers.change_series'
        context['delete'] = 'refers.delete_series'
        context['perm'] = self.request.user.get_all_permissions()
        return context

class SeriesCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'refers.add_series'
    model = Series
    form_class = SeriesForm
    success_url = '/series/list'
    template_name = "mainCRUD/create.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Создание объекта: серия'
        context['action'] = 'Введение информации о новой серии'
        context['current_menu'] = 'Серии'
        context['url'] = '/series'
        return context

class SeriesUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'refers.change_series'
    model = Series
    form_class = SeriesForm
    success_url = '/series/list'
    template_name = "mainCRUD/create.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Редактирование объекта: серия'
        context['action'] = 'Редактирование существующего объекта: серия'
        context['current_menu'] = 'Серии'
        context['url'] = '/series'
        return context

class SeriesDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'refers.delete_series'
    model = Series
    success_url = '/series/list'
    template_name = "mainCRUD/delete.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Удаление объекта: серия'
        context['action'] = 'Удаление существующего объекта: серия'
        context['current_menu'] = 'Серии'
        context['url'] = '/series'
        return context

class SeriesViewSet(viewsets.ModelViewSet):
    queryset = Series.objects.all()
    serializer_class = SeriesSerializer


# AUTHOR CRUD
class AuthorListView(ListView):
    model = Author
    template_name = "refers/author/list.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Список авторов'
        context['action'] = 'СПИСОК АВТОРОВ'
        context['current_menu'] = 'Авторы'
        context['url'] = '/author'
        context['add'] = 'refers.add_author'
        context['change'] = 'refers.change_author'
        context['delete'] = 'refers.delete_author'
        context['perm'] = self.request.user.get_all_permissions()
        return context

class AuthorDetailView(DetailView):
    model = Author
    template_name = "refers/author/detail.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Детали объекта: автор'
        context['action'] = 'Детализированная информация об авторе'
        context['current_menu'] = 'Авторы'
        context['url'] = '/author'
        context['add'] = 'refers.add_author'
        context['change'] = 'refers.change_author'
        context['delete'] = 'refers.delete_author'
        context['perm'] = self.request.user.get_all_permissions()
        return context

class AuthorCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'refers.add_author'
    model = Author
    form_class = AuthorForm
    template_name = "mainCRUD/create.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Создание объекта: автор'
        context['action'] = 'Введение информации о новом авторе'
        context['current_menu'] = 'Авторы'
        context['url'] = '/author'
        return context

class AuthorUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'refers.update_author'
    model = Author
    form_class = AuthorForm
    template_name = "mainCRUD/create.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Редактирование объекта: автор'
        context['action'] = 'Редактирование существующего объекта: автор'
        context['current_menu'] = 'Авторы'
        context['url'] = '/author'
        return context

class AuthorDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'refers.delete_author'
    model = Author
    success_url = '/author/list'
    template_name = "mainCRUD/delete.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Удаление объекта: автор'
        context['action'] = 'Удаление существующего объекта: автор'
        context['current_menu'] = 'Авторы'
        context['url'] = '/author'
        return context

class AuthorViewSet(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer


# PUBLISHER CRUD
class PublisherListView(ListView):
    model = Publisher
    template_name = "refers/publisher/list.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Список издательств'
        context['action'] = 'СПИСОК ИЗДАТЕЛЬСТВ'
        context['current_menu'] = 'Издательства'
        context['url'] = '/publisher'
        context['add'] = 'refers.add_publisher'
        context['change'] = 'refers.change_publisher'
        context['delete'] = 'refers.delete_publisher'
        context['perm'] = self.request.user.get_all_permissions()
        return context

class PublisherDetailView(DetailView):
    model = Publisher
    template_name = "refers/publisher/detail.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Детали объекта: издательство'
        context['action'] = 'Детализированная информация об издательстве'
        context['current_menu'] = 'Издательства'
        context['url'] = '/publisher'
        context['add'] = 'refers.add_publisher'
        context['change'] = 'refers.change_publisher'
        context['delete'] = 'refers.delete_publisher'
        context['perm'] = self.request.user.get_all_permissions()
        return context

class PublisherCreateView(PermissionRequiredMixin, CreateView):
    permission_required = 'refers.add_publisher'
    model = Publisher
    form_class = PublisherForm
    success_url = '/publisher/list'
    template_name = "mainCRUD/create.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Создание объекта: издательство'
        context['action'] = 'Введение информации о новом издательстве'
        context['current_menu'] = 'Издательства'
        context['url'] = '/publisher'
        return context

class PublisherUpdateView(PermissionRequiredMixin, UpdateView):
    permission_required = 'refers.update_publisher'
    model = Publisher
    form_class = PublisherForm
    success_url = '/publisher/list'
    template_name = "mainCRUD/create.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Редактирование объекта: издательство'
        context['action'] = 'Редактирование существующего объекта: издательство'
        context['current_menu'] = 'Издательства'
        context['url'] = '/publisher'
        return context

class PublisherDeleteView(PermissionRequiredMixin, DeleteView):
    permission_required = 'refers.delete_publisher'
    model = Publisher
    success_url = '/publisher/list'
    template_name = "mainCRUD/delete.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Удаление объекта: издательство'
        context['action'] = 'Удаление существующего объекта: издательство'
        context['current_menu'] = 'Издательства'
        context['url'] = '/publisher'
        return context

class PublisherViewSet(viewsets.ModelViewSet):
    queryset = Publisher.objects.all()
    serializer_class = PublisherSerializer