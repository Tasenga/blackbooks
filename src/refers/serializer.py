from .models import Genre, Author, Series, Publisher
from rest_framework import serializers


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ['pk', 'name']


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ['pk', 'name', 'surname']


class SeriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Series
        fields = ['pk', 'name']


class PublisherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publisher
        fields = ['pk', 'name']